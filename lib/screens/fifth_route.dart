import 'package:flutter/material.dart';

class FifthRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Fifth Screen"),
      ),
      body: Center(
        child: RaisedButton(
          color: Colors.blueGrey,
          onPressed: () {
            Navigator.of(context)
                .pushNamedAndRemoveUntil('/sixth', ModalRoute.withName('/'));
            // Navigator.of(context)
            //    .pushNamedAndRemoveUntil('/sixth', (route) => false);
          },
          child: Text('Go back'),
        ),
      ),
    );
  }
}
