import 'package:flutter/material.dart';

class FourthRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Fourth Screen"),
      ),
      body: Center(
        child: RaisedButton(
          color: Colors.blueGrey,
          onPressed: () {
            Navigator.of(context).pushNamed('/fifth');
          },
          child: Text('Next S5'),
        ),
      ),
    );
  }
}
