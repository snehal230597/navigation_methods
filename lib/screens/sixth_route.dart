import 'package:flutter/material.dart';

class SixthRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Logout Screen"),
      ),
      body: Center(
        child: RaisedButton(
          color: Colors.blueGrey,
          onPressed: () {
            Navigator.popUntil(context, ModalRoute.withName('/'));
            // Navigator.pop(context);
            // Navigator.popUntil(context, (route) => false);
          },
          child: Text('Go back'),
        ),
      ),
    );
  }
}
