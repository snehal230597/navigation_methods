import 'package:flutter/material.dart';

class ThirdRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Third Screen"),
      ),
      body: Center(
        child: RaisedButton(
          color: Colors.blueGrey,
          onPressed: () {
            // Navigator.of(context).pushNamed('/fourth');
            Navigator.of(context).pushReplacementNamed('/fourth');
            //  Navigator.of(context).popAndPushNamed('/fourth');
          },
          child: Text('Next S4'),
        ),
      ),
    );
  }
}
