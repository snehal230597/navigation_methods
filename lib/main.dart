import 'package:flutter/material.dart';

import 'screens/fifth_route.dart';
import 'screens/first_route.dart';
import 'screens/fourth_route.dart';
import 'screens/second_route.dart';
import 'screens/sixth_route.dart';
import 'screens/third_route.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Navigation Methods',
      initialRoute: '/',
      routes: {
        '/': (context) => FirstRoute(),
        '/second': (context) => SecondRoute(),
        '/third': (context) => ThirdRoute(),
        '/fourth': (context) => FourthRoute(),
        '/fifth': (context) => FifthRoute(),
        '/sixth': (context) => SixthRoute()
      },
    );
  }
}

// class FirstRoute extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('First Screen'),
//       ),
//       body: Center(
//         child: RaisedButton(
//           child: Text('Click Here'),
//           color: Colors.orangeAccent,
//           onPressed: () {
//             //  Navigator.maybePop(context);

//             //  Navigator.pushNamed(context, '/second');
//             Navigator.push(context,
//                 MaterialPageRoute(builder: (context) => SecondRoute()));
//           },
//         ),
//       ),
//     );
//   }
// }

// class SecondRoute extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Second Screen"),
//       ),
//       body: Center(
//         child: RaisedButton(
//           color: Colors.blueGrey,
//           onPressed: () {
//             Navigator.of(context).pushNamed('/third');
//           },
//           child: Text('Next S3'),
//         ),
//       ),
//     );
//   }
// }

// class ThirdRoute extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Third Screen"),
//       ),
//       body: Center(
//         child: RaisedButton(
//           color: Colors.blueGrey,
//           onPressed: () {
//             // Navigator.of(context).pushNamed('/fourth');
//             Navigator.of(context).pushReplacementNamed('/fourth');
//             //  Navigator.of(context).popAndPushNamed('/fourth');
//           },
//           child: Text('Next S4'),
//         ),
//       ),
//     );
//   }
// }

// class FourthRoute extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Fourth Screen"),
//       ),
//       body: Center(
//         child: RaisedButton(
//           color: Colors.blueGrey,
//           onPressed: () {
//             Navigator.of(context).pushNamed('/fifth');
//           },
//           child: Text('Next S5'),
//         ),
//       ),
//     );
//   }
// }

// class FifthRoute extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Fifth Screen"),
//       ),
//       body: Center(
//         child: RaisedButton(
//           color: Colors.blueGrey,
//           onPressed: () {
//             Navigator.of(context)
//                 .pushNamedAndRemoveUntil('/sixth', ModalRoute.withName('/'));
//             // Navigator.of(context)
//             //    .pushNamedAndRemoveUntil('/sixth', (route) => false);
//           },
//           child: Text('Go back'),
//         ),
//       ),
//     );
//   }
// }

// class SixthRoute extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Logout Screen"),
//       ),
//       body: Center(
//         child: RaisedButton(
//           color: Colors.blueGrey,
//           onPressed: () {
//             Navigator.popUntil(context, ModalRoute.withName('/'));
//             // Navigator.pop(context);
//             // Navigator.popUntil(context, (route) => false);
//           },
//           child: Text('Go back'),
//         ),
//       ),
//     );
//   }
// }
